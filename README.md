# Introducción

Hola! 😀 Bienvenidos al repositorio adjunto al microcurso de buenas prácticas de desarrollo de software. 

Soy Agustín Bernardo, ~~su IB17 favorito~~. En el breve lapso de tiempo que llevo en la industria aprendí una cosa o dos de buenas prácticas de desarrollo de software. Este documento intenta (y espero, no falla) hacer un instructivo de uso de lo que les voy a comentar en el curso.

### Prerrequisitos

Para entender de qué voy a estar hablando, espero tengan:
- Alguna idea de cómo funciona git 
- Sepan escribir programas en python


## Resumen

En este instructivo vamos a tratar de resolver los siguientes temas:
- Agile methodologies:
  - Crear un repositorio
  - Crear un plan de trabajo bajo metodologías ágiles
  - Dividir ese plan de trabajos en tareas, estimarlas y asignarlas
  - Crear branches según la metodología [gitflow](https://nvie.com/posts/a-successful-git-branching-model/)
  - Atacar un issue, escribir código, y...
- Testing:
  - Qué es TDD (test-driven development) y algunos conceptos detrás
  - Frameworks de testing (al menos en python)
  - Testing automático y ejemplo sobre el código
- Documentación:
  - Frameworks de documentación (e.g. Sphinx)
  - Documentación sobre el código
  - Generación automática a partir del código 
- DevOps (CI/CD):
  - Docker, y containerización de aplicaciones 
  - Cómo organizar los pasos anteriores
  - Jobs, stages y runners
  - GitLab pages

## Metodologías ágiles

## Testing

## Documentación

## CI/CD