from ..numerical.operations import operate


def test_sum():
    assert operate(1,2,"Addition") == 3
    
def test_div():
    assert operate(1,2,"Division") == 0.5
    
def test_mult():
    assert operate(1,2,"Multiplication") == 2
    
def test_sub():
    assert operate(1,2,"Subtraction") == -1
