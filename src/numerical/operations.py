def operate(var_1, var_2, operation):
    """
    Given an operation and two variables, returns the result:
    
    R = op(a, b)

    Parameters
    ----------
    var_1: float
           first variable to operate with
    var_2: float
            second variable to operate with
    operation: string
            name of the operation to use
    Returns
    -------
    result: float
            result of operation between var_1 and var_2
    """

    if(operation == 'Addition'):
        result = var_1 + var_2
    elif(operation == 'Subtraction'):
        result = var_1 - var_2
    elif(operation == 'Multiplication'):
        result = var_1 * var_2
    elif(operation == 'Division'):
        result = var_1 / var_2
    elif(operation == 'Power'):
        result = var_1 ** var_2
    else:
        result = 'INVALID CHOICE'
    return result