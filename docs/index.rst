.. dev-best-practices documentation master file, created by
   sphinx-quickstart on Mon May  3 17:52:03 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dev-best-practices's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
